import { Component, Input } from '@angular/core';
import { PokemonStat } from '../../models/pokemon-detail.model';

@Component({
  selector: 'app-pokemon-detail-stats',
  templateUrl: './pokemon-detail-stats.component.html',
  styleUrls: ['./pokemon-detail-stats.component.scss']
})
export class PokemonDetailStatsComponent {

  @Input() stats: PokemonStat[] = [];

}
