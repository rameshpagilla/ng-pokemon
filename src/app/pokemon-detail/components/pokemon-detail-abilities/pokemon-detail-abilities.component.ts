import { Component, Input } from '@angular/core';
import { PokemonAbilityDetail } from '../../models/pokemon-ability-detail.model';

@Component({
  selector: 'app-pokemon-detail-abilities',
  templateUrl: './pokemon-detail-abilities.component.html',
  styleUrls: [ './pokemon-detail-abilities.component.scss' ]
})
export class PokemonDetailAbilitiesComponent {
  @Input() abilities: PokemonAbilityDetail[] = [];
}
