export interface PokemonDetail {
  id: number;
  base_experience: number;
  height: number;
  name: string;
  order: number;
  weight: number;
  abilities: PokemonAbility[];
  moves: PokemonMove[];
  stats: PokemonStat[];
  types: PokemonType[];

  image?: string;
}

export interface PokemonMeta {
  name: string;
  url: string;
}

export interface PokemonType {
  slot: number;
  type: PokemonMeta;
}

export interface PokemonStat {
  base_stat: number;
  effort: number;
  stat: PokemonMeta;
}

export interface PokemonAbility {
  ability: PokemonMeta;
  is_hidden: boolean;
  slot: number;
}

export interface PokemonMove {
  move: PokemonMeta;
}
