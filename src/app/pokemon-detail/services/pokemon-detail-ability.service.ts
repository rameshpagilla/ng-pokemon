import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { PokemonAbility } from '../models/pokemon-detail.model';
import { forkJoin, of } from 'rxjs';
import { PokemonAbilityDetail } from '../models/pokemon-ability-detail.model';
import { environment } from '../../../environments/environment';
import { POKEMON_DETAIL_ABILITY_MOCK } from '../mocks/pokemon-detail-ability.mock';
import { PokemonDetailAbilityCacheService } from './pokemon-detail-ability-cache.service';

@Injectable({
  providedIn: 'root'
})
export class PokemonDetailAbilityService {

  private _abilities: PokemonAbilityDetail[] = [];
  private _error: string = '';

  constructor(
    private readonly http: HttpClient,
    private readonly pokemonAbilityCacheService: PokemonDetailAbilityCacheService) {
  }

  public tryFromCache(abilityIdArray: number[]): PokemonAbilityDetail[] {
    const cached: PokemonAbilityDetail[] = [];
    abilityIdArray.forEach(id => {
      const ability: PokemonAbilityDetail | undefined = this.pokemonAbilityCacheService.getFromCache(id);
      if (ability) {
        cached.push(ability);
      }
    });
    return cached;
  }

  public fetchAbilities(abilityIdArray: number[]): void {

    this._abilities = [];
    const cachedAbilities: PokemonAbilityDetail[] = this.tryFromCache(abilityIdArray);

    if (cachedAbilities.length === abilityIdArray.length) {
      this._abilities = cachedAbilities;
      return;
    }

    const requests$ = environment.useMocks ?
      of(POKEMON_DETAIL_ABILITY_MOCK) :
      abilityIdArray.map(id => {
        return this.http.get<PokemonAbilityDetail>(`https://pokeapi.co/api/v2/ability/${id}`);
      });

    forkJoin(requests$)
      .subscribe(
        (abilities: PokemonAbilityDetail[]) => {
          this._abilities = abilities.map((ability: PokemonAbilityDetail) => {
            return {
              ...ability,
              effect_entries: ability.effect_entries.filter(entry => entry.language.name === 'en')
            };
          });

          this.pokemonAbilityCacheService.addToCache(this._abilities);
        },
        (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      );

  }

  get abilities(): PokemonAbilityDetail[] {
    return this._abilities;
  }

  get error(): string {
    return this._error;
  }

}
