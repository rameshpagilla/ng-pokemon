import { Injectable } from '@angular/core';
import { PokemonDetail } from '../models/pokemon-detail.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonDetailCacheService {
  private _cacheLimit: number = 50;
  private _pokemonDetailCache: PokemonDetail[] = [];

  public addToCache(pokemon: PokemonDetail): void {
    // Avoid duplication of pokemon cache.
    const isInCache = Boolean(this._pokemonDetailCache.find(cachedPokemon => {
      return cachedPokemon.id === pokemon.id;
    }));

    if (!isInCache) {
      this._pokemonDetailCache.push(pokemon);
      this._pokemonDetailCache = this._pokemonDetailCache.slice(0, this._cacheLimit);
    }
  }

  public getFromCache(name: string): PokemonDetail | undefined {
    return this._pokemonDetailCache.find((pokemon: PokemonDetail) => {
      return pokemon.name.toLowerCase() === name.toLowerCase();
    });
  }
}
