export enum PaginationDirection {
  Initialize,
  Next,
  Previous
}
