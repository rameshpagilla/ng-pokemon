export enum AppRoutes {
  Login = 'login',
  Catalogue = 'catalogue',
  TrainerProfile = 'trainer',
  SearchCatalogue = 'search'
}
