export class UrlUtil {
  static sliceIdFromUrl(url: string): number {
    return Number(url.split('/').filter(Boolean).pop());
  }
}
