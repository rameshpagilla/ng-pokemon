import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Pokemon } from '../../../pokemon-catalogue/models/pokemon.model';
import { TrainerCollectionService } from '../../services/trainer-collection.service';

@Component({
  selector: 'app-trainer-collection',
  templateUrl: './trainer-collection.component.html',
  styleUrls: [ './trainer-collection.component.scss' ],
  // Avoid duplicate execution in filteredPokemon when searchText is updated.
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrainerCollectionComponent {

  private searchText: string = '';

  constructor(private readonly collectionService: TrainerCollectionService) {
  }

  onSearchKeyUp(event: any): void {
    this.searchText = event.target.value.trim();
  }

  filteredPokemon(): Pokemon[] {
    return this.collectionService.collection
      .filter(pokemon => pokemon.name.toLowerCase().includes(this.searchText.toLowerCase()));
  }

}
