import { Component } from '@angular/core';
import { TrainerProfileService } from '../../services/trainer-profile.service';

@Component({
  selector: 'app-trainer-profile-page',
  templateUrl: './trainer-profile.page.html'
})
export class TrainerProfilePage {
  constructor(private readonly trainerService: TrainerProfileService) {
  }

  get trainerName(): string {
    return this.trainerService.getTrainerName();
  }
}
