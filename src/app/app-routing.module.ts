import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './login/pages/login/login.page';
import { TrainerProfilePage } from './trainer-profile/pages/trainer-profile/trainer-profile.page';
import { PokemonCataloguePage } from './pokemon-catalogue/pages/pokemon-catalogue/pokemon-catalogue-page.component';
import { SearchCataloguePage } from './search-catalogue/pages/search-catalogue/search-catalogue.page';
import { PokemonDetailPage } from './pokemon-detail/pages/pokemon-detail/pokemon-detail.page';
import { AppRoutes } from './shared/enums/app-routes.enum';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: `/${AppRoutes.Login}`
  },
  {
    path: AppRoutes.Login,
    component: LoginPage
  },
  {
    path: AppRoutes.TrainerProfile,
    component: TrainerProfilePage
  },
  {
    path: AppRoutes.Catalogue,
    pathMatch: 'full',
    component: PokemonCataloguePage
  },
  {
    path: `${AppRoutes.Catalogue}/:name`,
    component: PokemonDetailPage
  },
  {
    path: AppRoutes.SearchCatalogue,
    component: SearchCataloguePage
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
