import { Injectable } from '@angular/core';
import { PaginationDirection } from '../../shared/enums/pagination-direction.enum';

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueApiService {
  private _initialized: boolean = false;
  private _next: string = 'https://pokeapi.co/api/v2/pokemon?offset=0&limit=20';
  private _offset: number = 0;
  private _limit: number = 20;

  public createRequestUrl(direction: PaginationDirection): string {

    let url: string = '';

    switch (direction) {
      case PaginationDirection.Initialize:
        this._initialized = true;
        url = this._next;
        break;
      case PaginationDirection.Next:
        this._offset += this._limit;
        url = `https://pokeapi.co/api/v2/pokemon?offset=${this._offset}&limit=${this._limit}`;
        break;
      case PaginationDirection.Previous:
        this._offset = this._offset - this._limit;
        if (this._offset < 0) {
          this._offset = 0;
        }
        url = `https://pokeapi.co/api/v2/pokemon?offset=${this._offset}&limit=${this._limit}`;
        break;
      default: {
        url = this._next;
      }
    }

    return url;
  }

  public initialized(): boolean {
    return this._initialized;
  }

  get offset(): number {
    return this._offset;
  }

  get limit(): number {
    return this._limit;
  }
}
