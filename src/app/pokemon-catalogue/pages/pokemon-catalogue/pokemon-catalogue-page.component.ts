import { Component, OnInit } from '@angular/core';
import { PokemonCatalogueService } from '../../services/pokemon-catalogue.service';
import { Pokemon } from '../../models/pokemon.model';
import { PaginationDirection } from '../../../shared/enums/pagination-direction.enum';
import { Router } from '@angular/router';
import { AppRoutes } from '../../../shared/enums/app-routes.enum';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './pokemon-catalogue-page.component.html',
  styleUrls: [ './pokemon-catalogue-page.component.scss' ]
})
export class PokemonCataloguePage implements OnInit {
  constructor(private catalogueService: PokemonCatalogueService, private router: Router) {
  }

  ngOnInit(): void {
    if (!this.catalogueService.initialized()) {
      this.catalogueService.fetchPokemon(PaginationDirection.Initialize);
    }
  }

  get catalogue(): Pokemon[] {
    return this.catalogueService.catalogue();
  }

  public async onCatalogueItemClick(pokemon: Pokemon): Promise<void> {
    await this.router.navigate([ AppRoutes.Catalogue, pokemon.name.toLowerCase() ]);
  }

}
