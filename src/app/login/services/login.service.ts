import { Injectable } from '@angular/core';
import { LocalStorageUtil } from '../../shared/utils/local-storage.util';
import { StorageKeys } from '../../shared/enums/storage-keys.enum';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public login(trainerName: string): void {
    LocalStorageUtil.set(StorageKeys.Trainer, trainerName);
  }

}
