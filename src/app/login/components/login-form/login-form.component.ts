import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent {

  @Output() success: EventEmitter<void> = new EventEmitter<void>();

  constructor(private readonly loginService: LoginService) {
  }

  public loginSubmit(loginForm: NgForm): void {
    const { trainerName } = loginForm.value;
    this.loginService.login(trainerName);
    this.success.emit();
  }

}
