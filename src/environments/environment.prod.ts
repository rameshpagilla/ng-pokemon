export const environment = {
  production: true,
  useMocks: false,
  apiBaseUrl: 'https://pokeapi.co/api/v2/pokemon',
  imageBaseUrl: 'assets/pokemon/sprites'
};
